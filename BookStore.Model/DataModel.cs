﻿using BookStore.DAL;
using BookStore.Model.Serialization;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Model
{
    [DataContract]
    public class DataModel : IDataModel
    {
        readonly BookstoreContext _ctx = new();

        public DataModel()
        {
            Authors = new CrudCollection<Author>(_ctx.Authors.Cast<Author>().ToList());
            Languages = new CrudCollection<Language>(_ctx.Languages.Cast<Language>().ToList());
            Books = new CrudCollection<Book>(_ctx.Books.Cast<Book>().ToList());

            Authors.OnAdd += GetHandler<Author, DAL.Author>(_ctx.Authors.Add, v => v);
            Authors.OnRemove += GetHandler<Author, DAL.Author>(
                item => _ctx.Authors.Remove(_ctx.Authors.Find(item.Id)),
                v => v
            );
            Authors.OnUpdate += GetHandler<Author, DAL.Author>(
                (item) =>
                {
                    _ctx.Entry(_ctx.Authors.Find(item.Id)).State = EntityState.Detached;
                    return _ctx.Authors.Update(item);
                },
                v => v
            );

            Languages.OnAdd += GetHandler<Language, DAL.Language>(_ctx.Languages.Add, v => v);
            Languages.OnRemove += GetHandler<Language, DAL.Language>(
                item => _ctx.Languages.Remove(_ctx.Languages.Find(item.Id)),
                v => v
            );
            Languages.OnUpdate += GetHandler<Language, DAL.Language>(
                (item) =>
                {
                    _ctx.Entry(_ctx.Languages.Find(item.Id)).State = EntityState.Detached;
                    return _ctx.Languages.Update(item);
                },
                v => v
            );

            Books.OnAdd += GetHandler<Book, DAL.Book>(_ctx.Books.Add, v => v);
            Books.OnRemove += GetHandler<Book, DAL.Book>(_ctx.Books.Remove, v => v);
            Books.OnUpdate += GetHandler<Book, DAL.Book>(
                (item) =>
                {
                    _ctx.Entry(_ctx.Books.Find(item.Id)).State = EntityState.Detached;
                    return _ctx.Books.Update(item);
                },
                v => v
            );
        }

        public static string FilePath { get; set; } = @"serialized data model.dat";

        [DataMember]
        public ICrudCollection<Author> Authors { get; set; }

        [DataMember]
        public ICrudCollection<Book> Books { get; set; }

        [DataMember]
        public ICrudCollection<Language> Languages { get; set; }

        public static DataModel? Load()
        {
            if (File.Exists(FilePath))
                return DataSerializer.Instance.DeserializeItem(FilePath);
            return new();
        }

        public void Save()
        {
            DataSerializer.Instance.SerializeData(FilePath, this);
        }

        EventHandler<TModel> GetHandler<TModel, TEntity>(
            Func<TEntity, EntityEntry<TEntity>> dbSetAction,
            Func<TModel, TEntity> converter
        ) where TEntity : class
        {
            return (_, item) =>
            {
                dbSetAction(converter(item));
                _ctx.SaveChanges();
            };
        }
    }
}
