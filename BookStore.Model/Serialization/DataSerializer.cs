﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Model.Serialization
{
    public class DataSerializer
    {
        static DataSerializer()
        {
            Instance = new();
        }

        private DataSerializer()
        {
        }

        public static DataSerializer Instance { get; private set; }

        public void SerializeData(string fileName, DataModel data)
        {
            DataContractSerializer formatter = new(typeof(DataModel));
            using FileStream s = new(fileName, FileMode.Create);

            formatter.WriteObject(s, data);
        }

        public DataModel? DeserializeItem(string fileName)
        {
            DataContractSerializer formatter = new(typeof(DataModel));
            using FileStream s = new(fileName, FileMode.Open);

            return formatter.ReadObject(s) as DataModel;
        }
    }
}
