﻿using System.Runtime.Serialization;

namespace BookStore.Model
{
    public interface IDataModel
    {
        ICrudCollection<Author> Authors { get; set; }

        ICrudCollection<Book> Books { get; set; }

        ICrudCollection<Language> Languages { get; set; }
    }
}
