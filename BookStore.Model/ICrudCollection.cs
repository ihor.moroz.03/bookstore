﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Model
{
    public interface ICrudCollection<T> : IEnumerable<T>
    {
        void Add(T item);

        void Update(T item);

        void Remove(T item);

        event EventHandler<T> OnAdd;

        event EventHandler<T> OnRemove;

        event EventHandler<T> OnUpdate;
    }
}
