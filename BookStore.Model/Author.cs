﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Model
{
    [DataContract]
    public class Author
    {
        [DataMember]
        public string? Name { get; set; }

        public int Id { get; init; }

        public override string ToString()
        {
            return Name ?? "";
        }

        public static implicit operator Author(DAL.Author author)
        {
            return new Author { Id = author.Id, Name = author.Name };
        }

        public static implicit operator DAL.Author(Author author)
        {
            return new DAL.Author() { Id = author.Id, Name = author.Name };
        }
    }
}
