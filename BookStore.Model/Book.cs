﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Model
{
    [DataContract]
    public class Book
    {
        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public int PublicationYear { get; set; }

        [DataMember]
        public Author Author { get; set; }

        [DataMember]
        public Language Language { get; set; }

        public int Id { get; init; }

        public static implicit operator Book(DAL.Book book)
        {
            return new Book
            {
                Id = book.Id,
                Title = book.Title,
                Author = book.Author,
                Language = book.Language,
                PublicationYear = book.PublicationYear,
            };
        }

        public static implicit operator DAL.Book(Book book)
        {
            return new DAL.Book()
            {
                Id = book.Id,
                Title = book.Title,
                AuthorId = book.Author.Id,
                LanguageId = book.Language.Id,
                PublicationYear = book.PublicationYear,
            };
        }
    }
}
