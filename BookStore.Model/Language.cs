﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Model
{
    [DataContract]
    public class Language
    {
        [DataMember]
        public string Code { get; set; }

        [DataMember]
        public string? Name { get; set; }

        public int Id { get; init; }

        public override string ToString()
        {
            return Code;
        }

        public static implicit operator Language(DAL.Language language)
        {
            return new Language
            {
                Id = language.Id,
                Code = language.Code,
                Name = language.Name
            };
        }

        public static implicit operator DAL.Language(Language language)
        {
            return new DAL.Language()
            {
                Id = language.Id,
                Code = language.Code,
                Name = language.Name
            };
        }
    }
}
