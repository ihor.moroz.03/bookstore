﻿using System;
using System.Collections.Generic;

namespace BookStore.DAL;

public partial class Language
{
    public int Id { get; set; }

    public string? Code { get; set; }

    public string? Name { get; set; }

    public virtual ICollection<Book> Books { get; } = new List<Book>();
}
