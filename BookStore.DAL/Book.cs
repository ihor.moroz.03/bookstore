﻿using System;
using System.Collections.Generic;

namespace BookStore.DAL;

public partial class Book
{
    public int Id { get; set; }

    public string? Title { get; set; }

    public int PublicationYear { get; set; }

    public int? AuthorId { get; set; }

    public int? LanguageId { get; set; }

    public virtual Author? Author { get; set; }

    public virtual Language? Language { get; set; }
}
