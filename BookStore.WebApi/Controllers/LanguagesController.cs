﻿using BookStore.Model;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BookStore.WebApi.Controllers
{
    [Route("languages")]
    [ApiController]
    public class LanguagesController : ControllerBase
    {
        readonly IDataModel _model;

        public LanguagesController(IDataModel model)
        {
            _model = model;
        }

        [HttpGet]
        public IEnumerable<Language> Get()
        {
            return _model.Languages;
        }

        // GET api/<BooksController>/5
        [HttpGet("{id}")]
        public Language? Get(int id)
        {
            return _model.Languages.FirstOrDefault(b => b.Id == id);
        }

        // POST api/<BooksController>
        [HttpPost]
        public void Post([FromBody] Language language)
        {
            _model.Languages.Add(language);
        }

        // PUT api/<BooksController>/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] Language? language)
        {
            if (id != language?.Id)
                return BadRequest();

            _model.Languages.Update(language);

            return Ok();
        }

        // DELETE api/<BooksController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            Language? language = _model.Languages.FirstOrDefault(b => b.Id == id);

            if (language != null)
                _model.Languages.Remove(language);
        }
    }
}
