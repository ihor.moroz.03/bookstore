﻿using BookStore.Model;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BookStore.WebApi.Controllers
{
    [Route("books")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        readonly IDataModel _model;

        public BooksController(IDataModel model)
        {
            _model = model;
        }

        // GET: api/<BooksController>
        [HttpGet]
        public IEnumerable<Book> Get()
        {
            return _model.Books;
        }

        // GET api/<BooksController>/5
        [HttpGet("{id}")]
        public Book? Get(int id)
        {
            return _model.Books.FirstOrDefault(b => b.Id == id);
        }

        // POST api/<BooksController>
        [HttpPost]
        public void Post([FromBody] Book book)
        {
            _model.Books.Add(book);
        }

        // PUT api/<BooksController>/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] Book? book)
        {
            if (id != book?.Id)
                return BadRequest();

            _model.Books.Update(book);

            return Ok();
        }

        // DELETE api/<BooksController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            Book? book = _model.Books.FirstOrDefault(b => b.Id == id);

            if (book != null)
                _model.Books.Remove(book);
        }
    }
}
