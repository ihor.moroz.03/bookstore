﻿using BookStore.Model;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BookStore.WebApi.Controllers
{
    [Route("authors")]
    [ApiController]
    public class AuthorsController : ControllerBase
    {
        readonly IDataModel _model;

        public AuthorsController(IDataModel model)
        {
            _model = model;
        }

        [HttpGet]
        public IEnumerable<Author> Get()
        {
            return _model.Authors;
        }

        // GET api/<BooksController>/5
        [HttpGet("{id}")]
        public Author? Get(int id)
        {
            return _model.Authors.FirstOrDefault(b => b.Id == id);
        }

        // POST api/<BooksController>
        [HttpPost]
        public void Post([FromBody] Author author)
        {
            _model.Authors.Add(author);
        }

        // PUT api/<BooksController>/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] Author? author)
        {
            if (id != author?.Id)
                return BadRequest();

            _model.Authors.Update(author);

            return Ok();
        }

        // DELETE api/<BooksController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            Author? author = _model.Authors.FirstOrDefault(b => b.Id == id);

            if (author != null)
                _model.Authors.Remove(author);
        }
    }
}
