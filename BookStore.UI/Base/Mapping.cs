﻿using AutoMapper;
using BookStore.Model;
using BookStore.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.UI.Base
{
    public static class Mapping
    {
        public static readonly MapperConfiguration Config = new(cfg =>
        {
            cfg.CreateMap<DataModel, DataViewModel>();
            cfg.CreateMap<DataViewModel, DataModel>();

            cfg.CreateMap<Book, BookViewModel>();
            cfg.CreateMap<BookViewModel, Book>();

            cfg.CreateMap<Author, AuthorViewModel>();
            cfg.CreateMap<AuthorViewModel, Author>();

            cfg.CreateMap<Language, LanguageViewModel>();
            cfg.CreateMap<LanguageViewModel, Language>();
        });
    }
}
