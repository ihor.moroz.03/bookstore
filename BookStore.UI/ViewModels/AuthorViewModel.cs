﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.UI.ViewModels
{
    public class AuthorViewModel : ViewModelBase
    {
        string _name;

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        public int Id { get; init; }

        public override string ToString()
        {
            return Name;
        }
    }
}
