﻿using BookStore.Model;
using BookStore.UI.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace BookStore.UI.ViewModels
{
    public class DataViewModel : ViewModelBase
    {
        Window? _addBookWindow;

        public DataViewModel()
        {
            SetShowAddBookWindow = new Command(ShowAddBookWindow);
            SetCancelAddBook = new Command(CancelAddBook);
            SetAddBook = new Command(AddBook);
        }

        ObservableCollection<AuthorViewModel> _authors;

        ObservableCollection<BookViewModel> _books;

        ObservableCollection<LanguageViewModel> _languages;

        public ObservableCollection<AuthorViewModel> Authors
        {
            get => _authors;
            set
            {
                _authors = value;
                OnPropertyChanged(nameof(Authors));
            }
        }

        public ObservableCollection<BookViewModel> Books
        {
            get => _books;
            set
            {
                _books = value;
                OnPropertyChanged("Books");
            }
        }

        public ObservableCollection<LanguageViewModel> Languages
        {
            get => _languages;
            set
            {
                _languages = value;
                OnPropertyChanged(nameof(Languages));
            }
        }

        public BookViewModel NewBook { get; init; } = new();

        public ICommand SetShowAddBookWindow { get; private set; }

        public ICommand SetCancelAddBook { get; private set; }

        public ICommand SetAddBook { get; private set; }

        void ShowAddBookWindow(object? parameter)
        {
            _addBookWindow ??= new AddBookWindow() { DataContext = this };
            _addBookWindow.Show();
        }

        void CancelAddBook(object? parameter)
        {
            _addBookWindow?.Close();
        }

        void AddBook(object? parameter)
        {
            Books.Add(NewBook);
            _addBookWindow?.Hide();
        }
    }
}
