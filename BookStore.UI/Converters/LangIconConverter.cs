﻿using BookStore.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace BookStore.UI.Converters
{
    public class LangIconConverter : IValueConverter
    {
        readonly Dictionary<string, BitmapImage> cache = new();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string lang = (value as LanguageViewModel)?.Code;

            if (!cache.ContainsKey(lang))
            {
                Uri uri =
                    new(
                        $@"{Environment.CurrentDirectory}\Images\Flags\{lang}.png",
                        UriKind.Absolute
                    );

                cache[lang] = new(uri);
            }

            return cache[lang];
        }

        public object ConvertBack(
            object value,
            Type targetType,
            object parameter,
            CultureInfo culture
        )
        {
            throw new NotImplementedException();
        }
    }
}
