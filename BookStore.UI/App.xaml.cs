﻿using AutoMapper;
using BookStore.Model;
using BookStore.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace BookStore.UI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        readonly DataViewModel _viewModel;
        readonly IMapper _mapper;
        DataModel? _model = new();

        public App()
        {
            _mapper = Base.Mapping.Config.CreateMapper();

            _viewModel = _mapper.Map<DataModel, DataViewModel>(_model);

            Window window = new MainWindow() { DataContext = _viewModel };
            window.Show();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            try
            {
                //_model = _mapper.Map<DataViewModel, DataModel>(_viewModel);
                //_model?.Save();
            }
            catch (Exception)
            {
                base.OnExit(e);
                throw;
            }
        }
    }
}
