using BookStore.Model.Serialization;

namespace BookStore.Model.Test
{
    [TestClass]
    public class Serialization
    {
        [TestMethod]
        public void TestMethodSerialise()
        {
            DataModel data = new();

            DataSerializer.Instance.SerializeData(@"serialized data model.dat", data);

            Assert.IsTrue(File.Exists(@"serialized data model.dat"));
        }

        [TestMethod]
        public void TestMethodDeserialize()
        {
            DataModel? data = DataSerializer.Instance.DeserializeItem(@"serialized data model.dat");

            Assert.IsNotNull(data);
        }
    }
}
